//dropDown

let dropDownBoxes = document.querySelectorAll('.headerDropDownBox');
dropDownBoxes.forEach(element => {
	let menu = element.getElementsByClassName('dropDownMenu');
	let arrow = element.getElementsByTagName('img');
	// console.log(arrow);
	element.addEventListener('click', function () {
		menu[0].classList.toggle('show');
		arrow[0].classList.toggle('rotate');
	});
});

// navLinks;
// let navs = document.getElementsByClassName('nav');
// navs.forEach(e => {
// 	e[0].addEventListener('click', () => {
// 		sideBar.classList.toggle('visible');
// 	});
// });

//sideBar

let hamberger = document.getElementById('hamberg');
let sideBar = document.getElementById('Navbar');

hamberger.addEventListener('click', function () {
	sideBar.classList.toggle('visible');
});

//carousel
let carouselContainer = document.querySelector('.scroll');
let btnContainer = document.querySelector('.scrollbuttonBox');
const left = btnContainer.querySelector('.left');
const right = btnContainer.querySelector('.right');
const itemsSize = Math.floor(
	carouselContainer.querySelectorAll('.item').length - 3
);
left.classList.add('disabled');
let x = 0;

function func() {
	if (x === 0) {
		left.classList.add('disabled');
	}

	if (x === itemsSize) {
		right.classList.add('disabled');
	}

	if (x > 0 && x < itemsSize) {
		left.classList.remove('disabled');
		right.classList.remove('disabled');
	}
}

left.addEventListener('click', function () {
	if (x > 0) {
		x--;
		func();
		carouselContainer.style.transform = `translateX(-${x * 31}%)`;
	}
});

right.addEventListener('click', function () {
	if (x < itemsSize) {
		x++;
		func();
		carouselContainer.style.transform = `translateX(-${x * 31}%)`;
	}
});

//sticky header
let prevScrollPos = window.pageYOffset;
let header = document.querySelector('.header');

window.addEventListener('scroll', function () {
	let currentScrollPos = window.pageYOffset;

	if (prevScrollPos < currentScrollPos) {
		header.classList.remove('up');
		header.classList.add('down');
	} else {
		header.classList.remove('down');
		header.classList.add('up');
	}

	prevScrollPos = currentScrollPos;
});

// accordian

let accordianBoxes = document.querySelectorAll('.accordianBox');

accordianBoxes.forEach(ele => {
	let matterBox = ele.getElementsByClassName('matterBox');
	let arrBox = ele.getElementsByClassName('arrowBox');
	ele.addEventListener('click', function () {
		matterBox[0].classList.toggle('show');
		arrBox[0].classList.toggle('rotate');
	});
});

//tabs
function openContent(evt, cityName) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName('tabcontent');
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = 'none';
	}
	tablinks = document.getElementsByClassName('tablinks');
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(' active', '');
	}
	document.getElementById(cityName).style.display = 'block';
	evt.currentTarget.className += ' active';
}
document.getElementById('defaultOpen').click();

//form
let email = document.getElementById('emailI');
let emailE = document.getElementById('emailError');

let name = document.getElementById('nameI');
let nameE = document.getElementById('nameError');

let company = document.getElementById('companyI');
let companyE = document.getElementById('companyError');

let country = document.getElementById('country');
let countryE = document.getElementById('countryError');

let button = document.getElementById('submit');

button.addEventListener('click', e => {
	e.preventDefault();

	validateAndDisplayError(email, emailE);
	validateAndDisplayError(name, nameE);
	validateAndDisplayError(company, companyE);
	validateAndDisplayError(country, countryE);
});

function validateAndDisplayError(inputElement, errorElement) {
	if (!inputElement.value) {
		errorElement.classList.add('showError');
	} else {
		errorElement.classList.remove('showError');
	}
}

//gif

// const gifImages = document.querySelectorAll('.gifImage');

// console.log(gifImages, 'kjhkj');

// function stopGifAnimation(image) {
// 	image.style.pointerEvents = 'none';
// 	image.style.animation = 'none';
// }

// function startGifAnimation(image) {
// 	image.style.pointerEvents = 'auto';
// 	image.style.animation = 'initial';
// }

// gifImages.forEach(image => {
// 	image.addEventListener('mouseenter', () => startGifAnimation(image));

// 	image.addEventListener('mouseleave', () => stopGifAnimation(image));

// 	stopGifAnimation(image);
// });
